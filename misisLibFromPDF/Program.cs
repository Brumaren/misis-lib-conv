﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Collections.Specialized;
using System.IO;
using System.Net.Http.Headers;

HttpClient h = new HttpClient();

var values = new Dictionary<string, string>
{
    { "action", "login" },
    { "cookieverify", "" },
    { "redirect", "" },
    { "username", $"{UserLogin()}" },
    { "password", $"{UserPass()}" },
    { "language", "ru_UN" }
};

h.DefaultRequestHeaders.Add("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9");
h.DefaultRequestHeaders.Add("Accept-Language", "ru,en;q=0.9,en-GB;q=0.8,en-US;q=0.7");
h.DefaultRequestHeaders.Add("Cache-Control", "max-age=0");
h.DefaultRequestHeaders.Add("Connection", "keep-alive");
h.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/x-www-form-urlencoded"));
h.DefaultRequestHeaders.Add("Upgrade-Insecure-Requests", "1");
h.DefaultRequestHeaders.Add("Origin", "http://elibrary.misis.ru");
h.DefaultRequestHeaders.Add("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/105.0.0.0 Safari/537.36 Edg/105.0.1343.27");
h.DefaultRequestHeaders.Add("Referer", "http://elibrary.misis.ru/login.php");
h.DefaultRequestHeaders.Add("Host", "elibrary.misis.ru");
h.DefaultRequestHeaders.Add("Accept-Charset", "ISO-8859-1");

var content = new FormUrlEncodedContent(values);
var response = await h.PostAsync("http://elibrary.misis.ru/login.php", content);
var responseString = await response.Content.ReadAsStringAsync();

List<Tuple<string, int>> docs = Docs();
int pageNum;


for (int i = 0; i < docs.Count; i++)
{
    if (!Directory.Exists("pdf-ки"))
    {
        Directory.CreateDirectory("pdf-ки");
    }

    pageNum = 0;
    Console.WriteLine();
    Document document = new Document();
    using (var pdfstream = new FileStream($"pdf-ки/{docs[i].Item1}-{docs[i].Item2}.pdf", FileMode.Create, FileAccess.Write, FileShare.None))
    {
        PdfWriter.GetInstance(document, pdfstream);
        document.Open();
        for (int j = 0; j < docs[i].Item2; j++)
        {
            await h.GetStringAsync($"http://elibrary.misis.ru/plugins/SecView/HashAvailability.php?id={docs[i].Item1}&page={pageNum}&type=small/fast");

            var test = await h.GetAsync($"http://elibrary.misis.ru/plugins/SecView/getDoc.php?id={docs[i].Item1}&page={pageNum}&type=small/fast");

            if (test.Content.Headers.ContentType.ToString() == "text/html; charset=utf-8")
            {
                Console.WriteLine($"{docs[i].Item1} - ошибка");
                break;
            }

            using (var imageStream = await h.GetStreamAsync($"http://elibrary.misis.ru/plugins/SecView/getDoc.php?id={docs[i].Item1}&page={pageNum}&type=small/fast"))
            {
                var image = Image.GetInstance(imageStream);
                document.Add(image);
                pageNum++;
                GetProgressLine(docs[i], pageNum);
            }
        }
        document.Close();
    }
}


Console.ReadLine();


void GetProgressLine(Tuple<string, int> doc, int page)
{
    Console.Write($"\r{doc.Item1}: [");
    for (int i = 0; i < 20; i++)
    {
        if ((double)page / (double)doc.Item2 * 20 > i) Console.Write("#");
        else Console.Write(".");
    }
    Console.Write("]");
}
string UserLogin()
{
    Console.Write("Введите логин: ");
    return Console.ReadLine();
}

string UserPass()
{
    Console.Write("Введите пароль: ");
    return Console.ReadLine();
}

List<Tuple<string, int>> Docs()
{
    Console.Write("Введите номер документа в формате (№-кол-во страниц; ...): ");
    var t = Console.ReadLine().Split(";");
    List<Tuple<string, int>> ans = new List<Tuple<string, int>>();
    for (int i = 0; i < t.Length; i++) ans.Add(new Tuple<string, int>(t[i].Split("-")[0], int.Parse(t[i].Split("-")[1])));
    return ans;
}